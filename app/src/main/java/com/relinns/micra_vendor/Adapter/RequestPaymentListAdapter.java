package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.EditRequetsPayment_Activity;
import com.relinns.micra_vendor.Activity.RequestMoney_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 07-07-2017.
 */

public class RequestPaymentListAdapter extends BaseAdapter {
    Context context;
    Activity activity;
    public RequestPaymentListAdapter(RequestMoney_Activity context) {
      this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.requestpayment_item, parent, false);
        holder.editTV=(LinearLayout)convertView.findViewById(R.id.editTV);
        holder.editTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, EditRequetsPayment_Activity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });



        return convertView;
    }

    public class Holder {
        LinearLayout editTV;

    }
}
