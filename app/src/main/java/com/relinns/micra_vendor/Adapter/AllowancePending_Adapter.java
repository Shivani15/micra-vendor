package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Activity.ModifyAllowance_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class AllowancePending_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public AllowancePending_Adapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.allowancepending_item, parent, false);

holder.modifyBT=(RelativeLayout)convertView.findViewById(R.id.modifyBT);
        holder.modifyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ModifyAllowance_Activity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        return convertView;
    }

    public class Holder {
        RelativeLayout modifyBT;

    }
}
