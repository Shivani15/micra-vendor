package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.OrderDetailAccepted_Activity;
import com.relinns.micra_vendor.Activity.OrderDetailComplete_Activity;
import com.relinns.micra_vendor.Activity.OrderDetailTransit_Activity;
import com.relinns.micra_vendor.Activity.OrderDetail_Pending_Activity;
import com.relinns.micra_vendor.Activity.OrderRefundRequest_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class MyOderList_Adapter extends BaseAdapter {
    Context context;
    Activity activity;

    public MyOderList_Adapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;


    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.myorderlist_item, parent, false);
        holder.prouctconditionTV=(TextView)convertView.findViewById(R.id.prouctconditionTV);
        holder.layotLL=(LinearLayout)convertView.findViewById(R.id.layotLL);
        if (position==0){
            holder.prouctconditionTV.setText("Pending");
        }
        if (position==1){
            holder.prouctconditionTV.setText("Accepted");
        }
      /*  if (position==2){
            holder.prouctconditionTV.setText("Completed");
        }
        if (position==3){
            holder.prouctconditionTV.setText("Transit");
        }
        if (position==4){
            holder.prouctconditionTV.setText("Refund Request");
        }*/

        holder.layotLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position==0){
                    Intent intent=new Intent(context, OrderDetail_Pending_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);

               }
                else{
                    Intent intent=new Intent(context, OrderDetailAccepted_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }


              /*  if (position==0){

                    Intent intent=new Intent(context, OrderDetail_Pending_Activity.class);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);

                }
              else  if (position==1){
                    Intent intent=new Intent(context, OrderDetailAccepted_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
               else if(position==2){
                    Intent intent=new Intent(context, OrderDetailComplete_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                else if (position==3){
                    Intent intent=new Intent(context, OrderDetailTransit_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                else{
                    Intent intent=new Intent(context, OrderRefundRequest_Activity.class);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }*/
            }
        });


        return convertView;
    }

    public class Holder {

        LinearLayout layotLL,editRL;
        TextView prouctconditionTV;
    }
}

