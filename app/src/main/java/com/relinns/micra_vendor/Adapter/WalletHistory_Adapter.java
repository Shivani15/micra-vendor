package com.relinns.micra_vendor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.EditProduct_Activity;
import com.relinns.micra_vendor.Activity.Product_Details;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class WalletHistory_Adapter extends BaseAdapter {
    Context context;

    public WalletHistory_Adapter(FragmentActivity context) {

        this.context = context;


    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.wallethistory_item, parent, false);


        return convertView;
    }

    public class Holder {

    }
}
