package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.Blocked_user_Activity;
import com.relinns.micra_vendor.Activity.ReportUser_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class BlockUsers_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public BlockUsers_Adapter(Blocked_user_Activity context) {
        this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.blockuser_item, parent, false);
        holder.reportuser_TV=(RelativeLayout)convertView.findViewById(R.id.reportuser_TV);
        holder.reportuser_TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ReportUser_Activity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });



        return convertView;
    }

    public class Holder {
        RelativeLayout reportuser_TV;

    }
}
