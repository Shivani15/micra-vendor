package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.relinns.micra_vendor.Activity.OrderDetailComplete_Activity;
import com.relinns.micra_vendor.DataModel.Homeaddress;
import com.relinns.micra_vendor.DataModel.StoreAddr;
import com.relinns.micra_vendor.Fragment.MyProfileFragment;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Relinns Technologies on 24-07-2017.
 */

public class AddressProfileList_Adapter extends BaseAdapter {
    Context context;
    AppCompatActivity activity;
    ArrayList<Homeaddress> addr=new ArrayList<>();
    ProgressDialog progressDialog;
    public AddressProfileList_Adapter(FragmentActivity context, ArrayList<Homeaddress> addr) {
        this.context=context;
        activity = (AppCompatActivity)context;

        this.addr=addr;


    }

    @Override
    public int getCount() {
        return addr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      Holder holder=new Holder();
        View rowView=null;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.address_list, parent, false);
        holder.frstaddTV=(TextView)convertView.findViewById(R.id.frstaddTV);
        holder.editRL=(LinearLayout)convertView.findViewById(R.id.editLL) ;
        holder.editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Work in Progress",
                        Toast.LENGTH_SHORT).show();
            }
        });

        holder.frstaddTV.setText(addr.get(position).getHomeaddr());
        holder.delete=(LinearLayout)convertView.findViewById(R.id.delete);
        holder.delete.setTag(position);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int clicked=v.getId();
                progressDialog = ProgressDialog.show(context, "", "Please wait");
                progressDialog.setCancelable(true);
                deleteaddr(addr.get(position).getAddr_id());

            }
        });





        return convertView;
    }

    public class Holder {

        LinearLayout layotLL,editRL,delete;
        TextView frstaddTV;
    }

    private void deleteaddr(String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);


        StringRequest postRequest = new StringRequest( Request.Method.DELETE, "http://apitest.micraapi.com/api/addresses/"+id,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        open("Address deleted!");

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //   Handle Error
                        progressDialog.dismiss();

                        Log.i("response","error"+volleyError);
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        open(message);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer "+ new GlobalData(context).gettoken());

                return headers;
            }
        };

        requestQueue.add(postRequest);
    }
    public void open(String msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(msg);
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Fragment favouriteFragment = new MyProfileFragment();
                                FragmentTransaction fragmentTransaction =activity.getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                                fragmentTransaction.commit();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}