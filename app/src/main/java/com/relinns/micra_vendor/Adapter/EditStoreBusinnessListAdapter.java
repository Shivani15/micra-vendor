package com.relinns.micra_vendor.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.icu.util.Calendar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.relinns.micra_vendor.Activity.AddNewProduct_Activity;
import com.relinns.micra_vendor.Activity.EditEmployee_Activity;
import com.relinns.micra_vendor.Activity.EditStore_Activity;
import com.relinns.micra_vendor.Activity.EmployDetailActivity;
import com.relinns.micra_vendor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Relinns Technologies on 25-07-2017.
 */

public class EditStoreBusinnessListAdapter extends BaseAdapter {
    Context context;
    int count;
    Holder holder;

    public EditStoreBusinnessListAdapter(EditStore_Activity context, int count) {
        this.context = context;
        this.count = count;

    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.bussiness_item, parent, false);

        holder.day_SP = (Spinner) convertView.findViewById(R.id.day_SP);
        holder.starttimeRL = (RelativeLayout) convertView.findViewById(R.id.starttimeRL);
        holder.endRL = (RelativeLayout) convertView.findViewById(R.id.endRL);
        holder.deleteIV = (ImageView) convertView.findViewById(R.id.deleteIV);
        holder.selecttime_TV = (TextView) convertView.findViewById(R.id.selecttime_TV);
        holder.endTV = (TextView) convertView.findViewById(R.id.endTV);
        selectday_spinner();



        holder.deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((EditStore_Activity)context).getdeletemethod();

            }
        });
        holder.starttimeRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                open_time(position);



            }
        });

        holder.endRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                end_time();
            }
        });

        return convertView;
    }

    private void open_time(final int position) {

                // Process to get Current Time
                final Calendar c = Calendar.getInstance();
             int   mHour = c.get(Calendar.HOUR_OF_DAY);
            int    mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                // Display Selected time in textbox
                              holder.selecttime_TV.setText(hourOfDay + ":" + minute);
                            }
                        }, mHour, mMinute, false);
                tpd.show();






    }


    private void end_time(){
      //  holder=new Holder();
        // Process to get Current Time
        final Calendar c = Calendar.getInstance();
        int   mHour = c.get(Calendar.HOUR_OF_DAY);
        int    mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog tpd = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        // Display Selected time in textbox
                        holder.endTV.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        tpd.show();


    }

    private void selectday_spinner() {
     //  holder.day_SP.getBackground().setColorFilter(context.getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        List<String> list = new ArrayList<String>();
        list.add("Sun");
        list.add("Mon");
        list.add("Tue");
        list.add("Wed");
        list.add("Thu");
        list.add("Fri");
        list.add("Sat");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.day_SP.setAdapter(dataAdapter);
        holder.day_SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public class Holder {
        RelativeLayout starttimeRL, endRL;
        Spinner day_SP;
        ImageView deleteIV;
        TextView selecttime_TV,endTV;

    }
}