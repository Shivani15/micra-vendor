package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.ChatList_Activity;
import com.relinns.micra_vendor.Activity.EditProduct_Activity;
import com.relinns.micra_vendor.Activity.Product_Details;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class ChatListAdapter extends BaseAdapter {
    Context context;
    Activity activity;

    public ChatListAdapter(ChatList_Activity context) {
        this.context = context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);

        if (position == 0) {
            convertView = inflater.inflate(R.layout.otherperson_message_item, parent, false);
        }
        if (position == 1) {
            convertView = inflater.inflate(R.layout.mymessage_design, parent, false);
        }
        if (position == 2) {
            convertView = inflater.inflate(R.layout.otherperson_message_item, parent, false);
        }
        if (position == 3) {
            convertView = inflater.inflate(R.layout.mymessage_design, parent, false);
        }
        return convertView;
    }

    public class Holder {

    }
}
