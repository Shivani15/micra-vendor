package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.OrderDetailComplete_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 24-07-2017.
 */

public class HistoryOrderList_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public HistoryOrderList_Adapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      Holder holder=new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.historylist_item, parent, false);
        holder.prouctconditionTV=(TextView)convertView.findViewById(R.id.prouctconditionTV);
        holder.layotLL=(LinearLayout)convertView.findViewById(R.id.layotLL);
        if (position==0){
            holder.prouctconditionTV.setText("Delivered");
        }
        if (position==1){
            holder.prouctconditionTV.setText("Delivered");
        }

        holder.layotLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, OrderDetailComplete_Activity.class);
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter,R.anim.exit);
            }
        });



        return convertView;
    }

    public class Holder {

        LinearLayout layotLL,editRL;
        TextView prouctconditionTV;
    }
}