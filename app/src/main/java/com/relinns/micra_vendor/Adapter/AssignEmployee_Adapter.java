package com.relinns.micra_vendor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.AssignEmployList_Activity;
import com.relinns.micra_vendor.Activity.EditProduct_Activity;
import com.relinns.micra_vendor.Activity.Product_Details;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class AssignEmployee_Adapter extends BaseAdapter {
    Context context;
    public AssignEmployee_Adapter(AssignEmployList_Activity context) {

        this.context=context;


    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.employeelist_item, parent, false);



        return convertView;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        RelativeLayout layotclickRL,editRL;
    }
}
