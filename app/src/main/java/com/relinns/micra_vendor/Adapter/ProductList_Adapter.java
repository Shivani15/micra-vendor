package com.relinns.micra_vendor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.AddStock_Activity;
import com.relinns.micra_vendor.Activity.EditProduct_Activity;
import com.relinns.micra_vendor.Activity.Product_Details;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 19-06-2017.
 */
public class ProductList_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    public ProductList_Adapter(FragmentActivity context) {
        this.context=context;
        activity=(Activity)context;
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.ptoductlist_item, parent, false);
        holder.layotclickRL=(RelativeLayout)convertView.findViewById(R.id.layotclickRL);
        holder.editRL=(RelativeLayout)convertView.findViewById(R.id.editRL);
        holder.adstockRL=(RelativeLayout)convertView.findViewById(R.id.adstockRL);
        holder.layotclickRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Product_Details.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        holder.editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, EditProduct_Activity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        holder.adstockRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, AddStock_Activity.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        return convertView;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        RelativeLayout layotclickRL,editRL,adstockRL;
    }
}
