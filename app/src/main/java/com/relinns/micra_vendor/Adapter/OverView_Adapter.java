package com.relinns.micra_vendor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.EditProduct_Activity;
import com.relinns.micra_vendor.Activity.Product_Details;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class OverView_Adapter  extends BaseAdapter {
    Context context;
    public OverView_Adapter(FragmentActivity context) {
        this.context=context;

    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.overviewlist_item, parent, false);
        holder.orderno_TV=(TextView)convertView.findViewById(R.id.orderno_TV);
        holder.date_TV=(TextView)convertView.findViewById(R.id.date_TV);
        holder.amount_TV=(TextView)convertView.findViewById(R.id.amount_TV);

        if (position==0){
            holder.orderno_TV.setText("Order No.");
            holder.date_TV.setText("Order Date");
            holder.amount_TV.setText("Earning");
        }




        return convertView;
    }

    public class Holder {
        TextView orderno_TV,date_TV,amount_TV;

    }
}
