package com.relinns.micra_vendor.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.Signup_Activity;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.model.CountryCode_Model;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Relinns Technologies on 25-07-2017.
 */

public class CountryCodeAdapter extends BaseAdapter {

    ArrayList<CountryCode_Model> items;
    Context context;
    private ArrayList<CountryCode_Model> arraystructlist;

    public CountryCodeAdapter(Signup_Activity context, ArrayList<CountryCode_Model> items) {
        this.context = context;
        this.items = items;
        this.arraystructlist = new ArrayList<CountryCode_Model>();
        this.arraystructlist.addAll(items);
       // this.arraystructlist.addAll(items);


    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        LayoutInflater inflater = LayoutInflater.from(context);

        convertView = inflater.inflate(R.layout.countrydesign_item, parent, false);

        holder.counTV = (TextView) convertView.findViewById(R.id.counTV);
        holder.codeTV = (TextView) convertView.findViewById(R.id.codeTV);
        holder.counTV.setText(items.get(position).getName());
        holder.codeTV.setText(items.get(position).getCode());


        return convertView;
    }

    public class Holder {

        TextView counTV, codeTV;

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        items.clear();
        if (charText.length() == 0) {
            items.addAll(arraystructlist);
        } else {
            for (CountryCode_Model wp : arraystructlist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                    items.add(wp);
                }

            }
        }
        notifyDataSetChanged();
    }
}