package com.relinns.micra_vendor.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.relinns.micra_vendor.Activity.AddProfile_Address_Activity;
import com.relinns.micra_vendor.Activity.AddStock_Activity;
import com.relinns.micra_vendor.Activity.Add_StoreAddress_Activity;
import com.relinns.micra_vendor.Activity.Login_Activity;
import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Activity.StoreDetail_Activity;
import com.relinns.micra_vendor.Adapter.AddressProfileList_Adapter;
import com.relinns.micra_vendor.DataModel.Homeaddress;
import com.relinns.micra_vendor.DataModel.Login;
import com.relinns.micra_vendor.DataModel.StoreAddr;
import com.relinns.micra_vendor.DataModel.UserDetails;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.relinns.micra_vendor.controller.RetrofitAdapter;
import com.relinns.micra_vendor.controller.RetrofitApiInterface;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {

    Button StoreLL, aboutLL;
    RelativeLayout addnewRL;
    TextView nameTV, emailET, numberTV;
    String token;

    ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myprofile_fragment, container, false);
        ((My_Products_Activity) getActivity()).settitleactivity("My Profile");
        StoreLL = (Button) view.findViewById(R.id.StoreLL);
        aboutLL = (Button) view.findViewById(R.id.aboutLL);
        nameTV = (TextView) view.findViewById(R.id.nameTV);
        emailET = (TextView) view.findViewById(R.id.emailET);
        numberTV = (TextView) view.findViewById(R.id.numberTV);


        StoreLL.setOnClickListener(this);
        aboutLL.setOnClickListener(this);
        token = new GlobalData(getActivity()).gettoken();
        //  addnewRL.setOnClickListener(this);
        profileApi();

        Fragment fm = new Only_ProfileFragment();
        FragmentTransaction ftr = getFragmentManager().beginTransaction();
        ftr.replace(R.id.profileframeFL, fm, null);

        ftr.commit();
        return view;
    }

    private void profileApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept","application/json");
        params.put("Authorization", "Bearer "+token);

        RetrofitApiInterface service = RetrofitAdapter.getRetrofitInstance().create(RetrofitApiInterface.class);

        Call<UserDetails> call = service.UserDetails(params);

        Log.wtf("request", call.request().url() + "");

        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
               // Log.d("API_Response", "UserDetail Response : " + new Gson().toJson(response.body()));
             //  Log.i("log", response.body());

                Log.d("Call request", call.request().toString());
                Log.d("Call request header", call.request().headers().toString());

                Log.d("Response raw header", response.headers().toString());
                Log.d("Response raw", String.valueOf(response.raw().body()));
                Log.d("Response code", String.valueOf(response.code()));


                if (response.isSuccessful()) {

                    Log.d("Response body", new Gson().toJson(response.body().toString()));

                   // Toast.makeText(getActivity(), "name  "+username, Toast.LENGTH_SHORT).show();
                    nameTV.setText(response.body().getName());
                    emailET.setText(response.body().getEmail());
                    numberTV.setText(response.body().getContactNumber());
                    new GlobalData(getActivity()).setUser_id(response.body().getId());

                } else {
                    Log.d("Response errorBody", String.valueOf(response.errorBody()));
                   // Log.d("API_Response", "UserDetail Response  : " + new Gson().toJson(response.errorBody()));
                }
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                Log.d("onFailure", t.toString());
                //t.getCause();

            }

        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.aboutLL:


                aboutLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                StoreLL.setBackgroundColor(getResources().getColor(R.color.white));
                aboutLL.setTextColor(getResources().getColor(R.color.white));
                StoreLL.setTextColor(getResources().getColor(R.color.black));


                Fragment fm = new Only_ProfileFragment();
                FragmentTransaction ftr = getFragmentManager().beginTransaction();
                ftr.replace(R.id.profileframeFL, fm, null);
                ftr.commit();


                break;
            case R.id.StoreLL:
                Toast.makeText(getActivity(), "Work in Progress",
                        Toast.LENGTH_SHORT).show();
//                StoreLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
//                aboutLL.setBackgroundColor(getResources().getColor(R.color.white));
//                StoreLL.setTextColor(getResources().getColor(R.color.white));
//                aboutLL.setTextColor(getResources().getColor(R.color.black));
//
//
//                Fragment fm1 = new Only_StoreDetailFragment();
//                FragmentTransaction ftr1 = getFragmentManager().beginTransaction();
//                ftr1.replace(R.id.profileframeFL, fm1, null);
//                ftr1.commit();

                break;

          /*  case R.id.addnewRL:
                Intent intent1=new Intent(getActivity(),AddProfile_Address_Activity.class);
                startActivity(intent1);

                break;*/
        }

    }


}
