package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.LeaveApplicationHistory_Adapter;
import com.relinns.micra_vendor.Adapter.LeaveApplicationPending_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class LeaveApplication_Fragment extends Fragment implements View.OnClickListener{
    TextView historyBT,pendingBT;
    ListView pendingLV,historyLV;
    LeaveApplicationHistory_Adapter leaveApplicationHistory_adapter;
    LeaveApplicationPending_Adapter leaveApplicationPending_adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.leaveapplication_fragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("Leave Application");
        historyBT=(TextView)view.findViewById(R.id.historyBT);
        pendingBT=(TextView)view.findViewById(R.id.pendingBT);

        pendingLV=(ListView)view.findViewById(R.id.pendingLV);
        historyLV=(ListView)view.findViewById(R.id.historyLV);

        leaveApplicationPending_adapter=new LeaveApplicationPending_Adapter(getActivity());
        pendingLV.setAdapter(leaveApplicationPending_adapter);

        leaveApplicationHistory_adapter=new LeaveApplicationHistory_Adapter(getActivity());
        historyLV.setAdapter(leaveApplicationHistory_adapter);


        historyBT.setOnClickListener(this);
        pendingBT.setOnClickListener(this);



        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.historyBT:

                pendingLV.setVisibility(View.GONE);
                historyLV.setVisibility(View.VISIBLE);



                historyBT.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                historyBT.setTextColor(getResources().getColor(R.color.white));

                pendingBT.setBackgroundColor(getResources().getColor(R.color.white));
                pendingBT.setTextColor(getResources().getColor(R.color.black));
                historyBT.setPadding(10, 10, 10, 10);
                pendingBT.setPadding(10, 10, 10, 10);

                break ;

            case R.id.pendingBT:

                pendingLV.setVisibility(View.VISIBLE);
                historyLV.setVisibility(View.GONE);

                pendingBT.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                pendingBT.setTextColor(getResources().getColor(R.color.white));

                historyBT.setBackgroundColor(getResources().getColor(R.color.white));
                historyBT.setTextColor(getResources().getColor(R.color.black));
                historyBT.setPadding(10, 10, 10, 10);
                pendingBT.setPadding(10, 10, 10, 10);

                break;
        }
    }
}
