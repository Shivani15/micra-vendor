package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class WebMicra_Fragment extends Fragment implements View.OnClickListener {




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.webmicra_fragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("Web Micra");

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


        }
    }
}
