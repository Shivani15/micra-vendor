package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.MessageList_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class MessageListFragment extends Fragment {
    ListView messageLV;
    MessageList_Adapter messageList_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.messagelistfragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("Message");

        messageLV=(ListView)view.findViewById(R.id.messageLV);

        messageList_adapter =new MessageList_Adapter(getActivity());
        messageLV.setAdapter(messageList_adapter);
        return view;
    }
}
