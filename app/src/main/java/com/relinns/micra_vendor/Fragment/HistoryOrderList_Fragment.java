package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra_vendor.Adapter.HistoryOrderList_Adapter;
import com.relinns.micra_vendor.Adapter.TransitorderListAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 24-07-2017.
 */

public class HistoryOrderList_Fragment extends Fragment {
    ListView orderLV;
    HistoryOrderList_Adapter myOderList_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.historylistfragment,container,false);

        orderLV=(ListView)view.findViewById(R.id.orderLV);


        myOderList_adapter=new HistoryOrderList_Adapter(getActivity());
        orderLV.setAdapter(myOderList_adapter);

        return view;
    }
}
