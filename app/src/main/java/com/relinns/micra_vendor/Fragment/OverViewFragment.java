package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.OverView_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class OverViewFragment extends Fragment implements View.OnClickListener{

    ListView orderLV;
    OverView_Adapter overView_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.overview_fragment,container,false);
        ((My_Products_Activity)getActivity()).settitleactivity("Overview");



        orderLV=(ListView)view.findViewById(R.id.orderLV);

        overView_adapter=new OverView_Adapter(getActivity());
        orderLV.setAdapter(overView_adapter);
        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
