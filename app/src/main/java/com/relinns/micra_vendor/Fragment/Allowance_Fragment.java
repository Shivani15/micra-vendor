package com.relinns.micra_vendor.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.AllowanceHistory_Adapter;
import com.relinns.micra_vendor.Adapter.AllowancePending_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class Allowance_Fragment extends Fragment implements View.OnClickListener {

    TextView pendingBT, historyBT;
    ListView pendingLV, historyLV;
    LinearLayout filterLL;

    AllowanceHistory_Adapter allowanceHistory_adapter;
    AllowancePending_Adapter allowancePending_adapter;
    SwipeRefreshLayout swiperefresh;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_allowance, container, false);
        ((My_Products_Activity) getActivity()).settitleactivity("Allowance");

        pendingBT = (TextView) view.findViewById(R.id.pendingBT);
        historyBT = (TextView) view.findViewById(R.id.historyBT);
        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        pendingLV = (ListView) view.findViewById(R.id.pendingLV);
        historyLV = (ListView) view.findViewById(R.id.historyLV);
        filterLL = (LinearLayout) view.findViewById(R.id.filterLL);

        pendingBT.setOnClickListener(this);
        historyBT.setOnClickListener(this);

        panding();

        allowanceHistory_adapter = new AllowanceHistory_Adapter(getActivity());
        historyLV.setAdapter(allowanceHistory_adapter);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefresh.setRefreshing(true);


                panding();

            }
        });


        return view;

    }

    private void panding() {



        allowancePending_adapter = new AllowancePending_Adapter(getActivity());
        pendingLV.setAdapter(allowancePending_adapter);
        swiperefresh.setRefreshing(false);
        //swiperefresh.setVisibility(View.GONE);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.historyBT:
                pendingLV.setVisibility(View.GONE);
                historyLV.setVisibility(View.VISIBLE);

                filterLL.setVisibility(View.VISIBLE);
                historyBT.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                historyBT.setTextColor(getResources().getColor(R.color.white));
                pendingBT.setBackgroundColor(getResources().getColor(R.color.white));
                pendingBT.setTextColor(getResources().getColor(R.color.black));
                historyBT.setPadding(10, 10, 10, 10);
                pendingBT.setPadding(10, 10, 10, 10);

                break;
            case R.id.pendingBT:

                filterLL.setVisibility(View.GONE);

                pendingLV.setVisibility(View.VISIBLE);
                historyLV.setVisibility(View.GONE);

                pendingBT.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                pendingBT.setTextColor(getResources().getColor(R.color.white));

                historyBT.setBackgroundColor(getResources().getColor(R.color.white));
                historyBT.setTextColor(getResources().getColor(R.color.black));
                historyBT.setPadding(10, 10, 10, 10);
                pendingBT.setPadding(10, 10, 10, 10);

                break;
        }

    }
}
