package com.relinns.micra_vendor.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.relinns.micra_vendor.Activity.AddProfile_Address_Activity;
import com.relinns.micra_vendor.Activity.Edit_Profile_Address;
import com.relinns.micra_vendor.Activity.Login_Activity;
import com.relinns.micra_vendor.Adapter.AddressProfileList_Adapter;
import com.relinns.micra_vendor.Adapter.HistoryOrderList_Adapter;
import com.relinns.micra_vendor.DataModel.BusinessHour;
import com.relinns.micra_vendor.DataModel.Homeaddress;
import com.relinns.micra_vendor.DataModel.StoreAddr;
import com.relinns.micra_vendor.DataModel.UserDetails;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.relinns.micra_vendor.controller.RetrofitAdapter;
import com.relinns.micra_vendor.controller.RetrofitApiInterface;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class Only_ProfileFragment extends Fragment implements View.OnClickListener {
    RelativeLayout addnewRL;
    LinearLayout editLL, editLL2;
    String token;
    List<BusinessHour> list;
    TextView frstaddTV;
    public ListView address_list;
    ProgressDialog progressDialog;
    ArrayList<Homeaddress> homeadd;
    public static AddressProfileList_Adapter addList_adapter;
    ProgressBar progressbar_loading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.onlyprofile_fragment, container, false);
        address_list=(ListView)view.findViewById(R.id.address_list);
        View footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);
        address_list.addFooterView(footerView);
        addnewRL = (RelativeLayout) view.findViewById(R.id.addnewRL);
        editLL = (LinearLayout) view.findViewById(R.id.editLL);
        addnewRL.setOnClickListener(this);
        homeadd=new ArrayList<>();
        token = new GlobalData(getActivity()).gettoken();
         progressbar_loading =  (ProgressBar)view.findViewById(R.id.progressbar_loading);
        progressbar_loading.setVisibility(View.VISIBLE);


       // profileApi();
        getAddressApi();

        return view;


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addnewRL:
                Intent intent1 = new Intent(getActivity(), AddProfile_Address_Activity.class);
                startActivityForResult(intent1,0);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.editLL:
                Intent intent2 = new Intent(getActivity(), Edit_Profile_Address.class);
                startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

           /* case R.id.editLL2:
                Intent intent3 = new Intent(getActivity(), Edit_Profile_Address.class);
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;*/
        }

    }

    private void profileApi() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Accept", "application/json");
        params.put("Authorization", "Bearer " + token);

        RetrofitApiInterface service = RetrofitAdapter.getRetrofitInstance().create(RetrofitApiInterface.class);

        Call<UserDetails> call = service.UserDetails(params);

        Log.wtf("request", call.request().url() + "");

        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                // Log.d("API_Response", "UserDetail Response : " + new Gson().toJson(response.body()));
                //  Log.i("log", response.body());

                Log.d("Call request", call.request().toString());
                Log.d("Call request header", call.request().headers().toString());

                Log.d("Response raw header", response.headers().toString());
                Log.d("Response raw", String.valueOf(response.raw().body()));
                Log.d("Response code", String.valueOf(response.code()));


                if (response.isSuccessful()) {

                    Log.d("Response body", new Gson().toJson(response.body().toString()));

                    response.body().getName();
                 //   String address=UserDetails.NewAddress
                   // list = response.body().getBusinessHour();
                   // frstaddTV.setText(list.get(1).getDay());

                } else {
                    Log.d("Response errorBody", String.valueOf(response.errorBody()));
                    // Log.d("API_Response", "UserDetail Response  : " + new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                Log.d("onFailure", t.toString());
                //t.getCause();

            }

        });


    }


    private void getAddressApi() {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());


        JsonArrayRequest postRequest = new JsonArrayRequest( com.android.volley.Request.Method.GET, "http://apitest.micraapi.com/api/addresses/user/",
                new com.android.volley.Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressbar_loading.setVisibility(View.GONE);
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj= response.getJSONObject(i);
                                String address="";
                                if(obj.has("storeName"))
                                {
                                    address= obj.getString("storeName")+","+obj.getString("landmark")+","+obj.getString("city")+","+obj.getString("state")+","+obj.getString("country")+","+obj.getString("pincode");
                                    StoreAddr str=new StoreAddr();
                                    str.setStoreName(address);
                                    new GlobalData(getActivity()).storeaddr.add(str);
                                    Collections.reverse(new GlobalData(getActivity()).storeaddr);
                                }else
                                {
                                    address= obj.getString("homeName")+","+obj.getString("landmark")+","+obj.getString("city")+","+obj.getString("state")+","+obj.getString("country")+","+obj.getString("pincode");
                                    Homeaddress home=new Homeaddress();
                                    home.setHomeaddr(address);
                                    home.setAddr_id(obj.getString("_id"));
                                    homeadd.add(home);
                                    Collections.reverse(homeadd);
                                }


                            }
                            Log.i("response", "size store" + new GlobalData(getActivity()).storeaddr.size());
                            Log.i("response", "size home" + homeadd);

                            addList_adapter = new AddressProfileList_Adapter(getActivity(),homeadd);
                            address_list.setAdapter(addList_adapter);
                            Log.i("response", "response" + response);
                        }catch (JSONException e)
                        {
                            Log.i("response", "error" + e);

                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //   Handle Error
                        progressbar_loading.setVisibility(View.GONE);

                        Log.i("response","error"+volleyError);
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Alerter.create(getActivity())
                                .setTitle("Micra")
                                .setText(message)
                                .setBackgroundColor(R.color.colorAccent)
                                .show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer "+ new GlobalData(getActivity()).gettoken());

                return headers;
            }
        };

        requestQueue.add(postRequest);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fm = new Only_ProfileFragment();
        FragmentTransaction ftr = getFragmentManager().beginTransaction();
        ftr.replace(R.id.profileframeFL, fm, null);

        ftr.commit();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
