package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.FavouriteBuyer_Adapter;
import com.relinns.micra_vendor.Adapter.FavouriteProduct_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class MyFavourite_Fragment extends Fragment implements View.OnClickListener {
    TextView favproductTV,favbuyerTV;
    ListView favproductLV,favbuyerLV;
    FavouriteBuyer_Adapter favouriteBuyer_adapter;
    FavouriteProduct_Adapter favouriteProduct_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myfavourite_fragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("Favourites");

        favbuyerTV=(TextView)view.findViewById(R.id.favbuyerTV);
        favproductTV=(TextView)view.findViewById(R.id.favproductTV);
        favproductLV=(ListView)view.findViewById(R.id.favproductLV);
        favbuyerLV=(ListView)view.findViewById(R.id.favbuyerLV);

        favbuyerTV.setOnClickListener(this);
        favproductTV.setOnClickListener(this);


        favouriteProduct_adapter=new FavouriteProduct_Adapter(getActivity());
        favproductLV.setAdapter(favouriteProduct_adapter);


        favouriteBuyer_adapter=new FavouriteBuyer_Adapter(getActivity());
        favbuyerLV.setAdapter(favouriteBuyer_adapter);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.favproductTV:



                favproductLV.setVisibility(View.VISIBLE);
                favbuyerLV.setVisibility(View.GONE);

                favproductTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                favproductTV.setTextColor(getResources().getColor(R.color.white));

                favbuyerTV.setBackgroundColor(getResources().getColor(R.color.white));
                favbuyerTV.setTextColor(getResources().getColor(R.color.black));
                favproductTV.setPadding(10, 10, 10, 10);
                favbuyerTV.setPadding(10, 10, 10, 10);

                break;
            case R.id.favbuyerTV:



                favproductLV.setVisibility(View.GONE);
                favbuyerLV.setVisibility(View.VISIBLE);
                favbuyerTV.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                favbuyerTV.setTextColor(getResources().getColor(R.color.white));
                favproductTV.setBackgroundColor(getResources().getColor(R.color.white));
                favproductTV.setTextColor(getResources().getColor(R.color.black));
                favproductTV.setPadding(10,10,10,10);
                favbuyerTV.setPadding(10,10,10,10);

                break;
        }

    }
}
