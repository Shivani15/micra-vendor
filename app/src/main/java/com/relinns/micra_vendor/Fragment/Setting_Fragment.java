package com.relinns.micra_vendor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Activity.Blocked_user_Activity;
import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Activity.NotificationList_Activity;
import com.relinns.micra_vendor.Activity.PermissionList_Activity;
import com.relinns.micra_vendor.Activity.ReportFeedback_Activity;
import com.relinns.micra_vendor.Activity.SubscriptionActivity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class Setting_Fragment extends Fragment implements View.OnClickListener {

    LinearLayout reportfeed_RL,blockuser_LL,permissionLL,subscriptionLL;
    RelativeLayout notificationRL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.settingfragment,container,false);
        ((My_Products_Activity)getActivity()).settitleactivity("Settings");

        reportfeed_RL=(LinearLayout)view.findViewById(R.id.reportfeed_RL);
        blockuser_LL=(LinearLayout)view.findViewById(R.id.blockuser_LL);
        permissionLL=(LinearLayout)view.findViewById(R.id.permissionLL);
        subscriptionLL=(LinearLayout)view.findViewById(R.id.subscriptionLL);
        notificationRL=(RelativeLayout)view.findViewById(R.id.notificationRL);
        reportfeed_RL.setOnClickListener(this);
        blockuser_LL.setOnClickListener(this);
        notificationRL.setOnClickListener(this);
        permissionLL.setOnClickListener(this);
        subscriptionLL.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reportfeed_RL:
                Intent intent=new Intent(getActivity(), ReportFeedback_Activity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.blockuser_LL:
                Intent intent1=new Intent(getActivity(), Blocked_user_Activity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.notificationRL:
                Intent intent2=new Intent(getActivity(), NotificationList_Activity.class);
                startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.permissionLL:
                Intent intent3=new Intent(getActivity(), PermissionList_Activity.class);
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.subscriptionLL:
                Intent intent4=new Intent(getActivity(), SubscriptionActivity.class);
                startActivity(intent4);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }

    }
}
