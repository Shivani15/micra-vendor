package com.relinns.micra_vendor.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.EmployeeListAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class MyEmployeeList_Fragment extends Fragment {
    ListView employeeLV;
    EmployeeListAdapter employeeListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.myemployee_fragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("My Employee");
        employeeLV=(ListView)view.findViewById(R.id.employeeLV);

        employeeListAdapter=new EmployeeListAdapter(getActivity());
        employeeLV.setAdapter(employeeListAdapter);

        return view;
    }
}
