package com.relinns.micra_vendor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.Add_StoreAddress_Activity;
import com.relinns.micra_vendor.Activity.EditStore_Activity;
import com.relinns.micra_vendor.Activity.StoreDetail_Activity;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class Only_StoreDetailFragment extends Fragment implements View.OnClickListener {
    TextView addnewTV,editTV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.only_storedetailfragment,container,false);

        addnewTV=(TextView)view.findViewById(R.id.addnewTV);
        editTV=(TextView)view.findViewById(R.id.editTV);

        addnewTV.setOnClickListener(this);
        editTV.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addnewTV:
                Intent intent=new Intent(getActivity(),Add_StoreAddress_Activity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.editTV:
                Intent intent1=new Intent(getActivity(),EditStore_Activity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

    }

    }
}
