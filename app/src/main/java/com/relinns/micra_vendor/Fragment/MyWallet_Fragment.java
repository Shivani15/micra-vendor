package com.relinns.micra_vendor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.AddNewBank_Activity;
import com.relinns.micra_vendor.Activity.AddNewCard_Activity;
import com.relinns.micra_vendor.Activity.EditBank_Activity;
import com.relinns.micra_vendor.Activity.EditCard_Activity;
import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Activity.RequestMoney_Activity;
import com.relinns.micra_vendor.Activity.SendMoney_Activity;
import com.relinns.micra_vendor.Activity.Topup_Activity;
import com.relinns.micra_vendor.Activity.Withdrawl_Activity;
import com.relinns.micra_vendor.Adapter.WalletHistory_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 06-07-2017.
 */

public class MyWallet_Fragment extends Fragment implements View.OnClickListener {

    LinearLayout overviewLL, historyLL, bankLL, over_LL;
    TextView overviewTV, historyTV, bankTV;
    ListView history_LV, bank_LV;
  WalletHistory_Adapter walletHistory_adapter;
    ScrollView banklayout;
    RelativeLayout addnewbank_RL,addnewcardRL,topupRL,sendmoneyRL,requestRL,withdralRL;
    LinearLayout editbankTV,editcardLL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mywallet_fragment, container, false);
        ((My_Products_Activity)getActivity()).settitleactivity("Wallet");
         overviewLL = (LinearLayout) view.findViewById(R.id.overviewLL);
        over_LL = (LinearLayout) view.findViewById(R.id.over_LL);
         historyLL = (LinearLayout) view.findViewById(R.id.historyLL);
         bankLL = (LinearLayout) view.findViewById(R.id.bankLL);
        editcardLL = (LinearLayout) view.findViewById(R.id.editcardLL);

        addnewbank_RL = (RelativeLayout) view.findViewById(R.id.addnewbank_RL);
        addnewcardRL = (RelativeLayout) view.findViewById(R.id.addnewcardRL);
        topupRL = (RelativeLayout) view.findViewById(R.id.topupRL);
        sendmoneyRL = (RelativeLayout) view.findViewById(R.id.sendmoneyRL);
        requestRL = (RelativeLayout) view.findViewById(R.id.requestRL);
        withdralRL = (RelativeLayout) view.findViewById(R.id.withdralRL);

        overviewTV = (TextView) view.findViewById(R.id.overviewTV);
        historyTV = (TextView) view.findViewById(R.id.historyTV);
        bankTV = (TextView) view.findViewById(R.id.bankTV);
        editbankTV = (LinearLayout) view.findViewById(R.id.editbankTV);
        history_LV = (ListView) view.findViewById(R.id.history_LV);
        banklayout=(ScrollView)view.findViewById(R.id.banklayout);

        walletHistory_adapter=new WalletHistory_Adapter(getActivity());
        history_LV.setAdapter(walletHistory_adapter);


        overviewLL.setOnClickListener(this);
        historyLL.setOnClickListener(this);
        bankLL.setOnClickListener(this);
        addnewbank_RL.setOnClickListener(this);
        editbankTV.setOnClickListener(this);
        addnewcardRL.setOnClickListener(this);
        topupRL.setOnClickListener(this);
        sendmoneyRL.setOnClickListener(this);
        requestRL.setOnClickListener(this);
        withdralRL.setOnClickListener(this);
        editcardLL.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.overviewLL:

                over_LL.setVisibility(View.VISIBLE);
                history_LV.setVisibility(View.GONE);
                banklayout.setVisibility(View.GONE);

                overviewLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                historyLL.setBackgroundColor(getResources().getColor(R.color.white));
                bankLL.setBackgroundColor(getResources().getColor(R.color.white));

                overviewTV.setTextColor(getResources().getColor(R.color.white));
                historyTV.setTextColor(getResources().getColor(R.color.black));
                bankTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.historyLL:

                over_LL.setVisibility(View.GONE);
                history_LV.setVisibility(View.VISIBLE);
                banklayout.setVisibility(View.GONE);

                historyLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                overviewLL.setBackgroundColor(getResources().getColor(R.color.white));
                bankLL.setBackgroundColor(getResources().getColor(R.color.white));

                historyTV.setTextColor(getResources().getColor(R.color.white));
                overviewTV.setTextColor(getResources().getColor(R.color.black));
                bankTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.bankLL:

                over_LL.setVisibility(View.GONE);
                history_LV.setVisibility(View.GONE);
                banklayout.setVisibility(View.VISIBLE);

                bankLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                overviewLL.setBackgroundColor(getResources().getColor(R.color.white));
                historyLL.setBackgroundColor(getResources().getColor(R.color.white));

                bankTV.setTextColor(getResources().getColor(R.color.white));
                overviewTV.setTextColor(getResources().getColor(R.color.black));
                historyTV.setTextColor(getResources().getColor(R.color.black));

                break;

            case R.id.addnewbank_RL:
                Intent intent=new Intent(getActivity(), AddNewBank_Activity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.editbankTV:
                Intent intent1=new Intent(getActivity(), EditBank_Activity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.addnewcardRL:
                Intent intent2=new Intent(getActivity(), AddNewCard_Activity.class);
                startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.topupRL:
                Intent intent3=new Intent(getActivity(), Topup_Activity.class);
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.sendmoneyRL:
                Intent intent4=new Intent(getActivity(), SendMoney_Activity.class);
                startActivity(intent4);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.requestRL:
                Intent intent5=new Intent(getActivity(),RequestMoney_Activity.class);
                startActivity(intent5);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.withdralRL:
                Intent intent6=new Intent(getActivity(),Withdrawl_Activity.class);
                startActivity(intent6);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);
                break;

            case R.id.editcardLL:
                Intent intent7=new Intent(getActivity(), EditCard_Activity.class);
                startActivity(intent7);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);



                break;

        }

    }
}
