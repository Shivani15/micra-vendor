package com.relinns.micra_vendor.Fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.relinns.micra_vendor.Activity.AddNewProduct_Activity;
import com.relinns.micra_vendor.Activity.My_Products_Activity;
import com.relinns.micra_vendor.Adapter.ProductList_Adapter;
import com.relinns.micra_vendor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Relinns Technologies on 19-06-2017.
 */
public class ProductList_Fragment extends Fragment implements View.OnClickListener {

    LinearLayout allproductLL,lowLL,stockLL;
    TextView allproductTV,lowTV,stockTV;
    Spinner categorySP;
    ListView productLV;
    ProductList_Adapter productList_adapter;
    Button adnew_BT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.productlist_fragment,container,false);
        ((My_Products_Activity) getActivity()).settitleactivity("My Products");
        allproductLL=(LinearLayout)view.findViewById(R.id.allproductLL);
        lowLL=(LinearLayout)view.findViewById(R.id.lowLL);
        stockLL=(LinearLayout)view.findViewById(R.id.stockLL);
        productLV=(ListView)view.findViewById(R.id.productLV);
        adnew_BT=(Button)view.findViewById(R.id.adnew_BT);

        allproductTV=(TextView)view.findViewById(R.id.allproductTV);
        lowTV=(TextView)view.findViewById(R.id.lowTV);
        stockTV=(TextView)view.findViewById(R.id.stockTV);
        categorySP = (Spinner)view.findViewById(R.id.categorySP);

        allproductLL.setOnClickListener(this);
        lowLL.setOnClickListener(this);
        stockLL.setOnClickListener(this);
        adnew_BT.setOnClickListener(this);

        selected_category_SP();
        productList_adapter=new ProductList_Adapter(getActivity());
        productLV.setAdapter(productList_adapter);



        return view;
    }

    private void selected_category_SP() {
           // categorySP.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            List<String> list = new ArrayList<String>();
            list.add("All Category");
            list.add("Normal");
            list.add("Express");
            list.add("Air");

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.newspinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySP.setAdapter(dataAdapter);
        categorySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }






    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.allproductLL:
                allproductLL.setBackground(getResources().getDrawable(R.drawable.designbutton));
                lowLL.setBackgroundColor(getResources().getColor(R.color.white));
                stockLL.setBackgroundColor(getResources().getColor(R.color.white));

                allproductTV.setTextColor(getResources().getColor(R.color.white));
                lowTV.setTextColor(getResources().getColor(R.color.black));
                stockTV.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.lowLL:

                allproductLL.setBackgroundColor(getResources().getColor(R.color.white));
                lowLL.setBackground(getResources().getDrawable(R.drawable.designbutton));
                stockLL.setBackgroundColor(getResources().getColor(R.color.white));

                allproductTV.setTextColor(getResources().getColor(R.color.black));
                lowTV.setTextColor(getResources().getColor(R.color.white));
                stockTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.stockLL:

                allproductLL.setBackgroundColor(getResources().getColor(R.color.white));
                lowLL.setBackgroundColor(getResources().getColor(R.color.white));
                stockLL.setBackground(getResources().getDrawable(R.drawable.designbutton));

                allproductTV.setTextColor(getResources().getColor(R.color.black));
                lowTV.setTextColor(getResources().getColor(R.color.black));
                stockTV.setTextColor(getResources().getColor(R.color.white));

                break;

            case R.id.adnew_BT:

                Intent intent=new Intent(getActivity(), AddNewProduct_Activity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter,R.anim.exit);

                break;
        }


    }
}
