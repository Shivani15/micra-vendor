package com.relinns.micra_vendor.model;

/**
 * Created by Relinns Technologies on 19-06-2017.
 */
public class NavigationHome_Model {
    int circle_large;

    public int getCircle_large() {
        return circle_large;
    }

    public void setCircle_large(int circle_large) {
        this.circle_large = circle_large;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public NavigationHome_Model(int circle_large, String home) {

        this.circle_large = circle_large;
        this.home = home;
    }

    String home;
}
