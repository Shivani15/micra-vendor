package com.relinns.micra_vendor.model;

/**
 * Created by Relinns-Technologies on 3/23/2017.
 */

public class image_model {

    int url;

    public image_model(int url) {
        this.url = url;
    }

    public int getUrl() {
        return url;
    }

    public void setUrl(int url) {
        this.url = url;
    }
}
