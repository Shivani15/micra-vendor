package com.relinns.micra_vendor.model;

/**
 * Created by Relinns Technologies on 25-07-2017.
 */

public class CountryCode_Model  {
    String name,  code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryCode_Model(String name, String code) {
        this.name=name;
        this.code=code;


    }

}
