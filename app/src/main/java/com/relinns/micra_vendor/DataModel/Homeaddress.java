package com.relinns.micra_vendor.DataModel;

/**
 * Created by Relinns Technologies on 19-08-2017.
 */

public class Homeaddress {
    public String getHomeaddr() {
        return homeaddr;
    }

    public void setHomeaddr(String homeaddr) {
        this.homeaddr = homeaddr;
    }

    public String homeaddr;

    public String getAddr_id() {
        return addr_id;
    }

    public void setAddr_id(String addr_id) {
        this.addr_id = addr_id;
    }

    public String addr_id;
}
