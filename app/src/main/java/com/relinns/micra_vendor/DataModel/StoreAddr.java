package com.relinns.micra_vendor.DataModel;

/**
 * Created by Relinns Technologies on 19-08-2017.
 */

public class StoreAddr {

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String StoreName;

}
