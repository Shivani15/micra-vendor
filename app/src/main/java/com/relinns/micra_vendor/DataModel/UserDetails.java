package com.relinns.micra_vendor.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Relinns Technologies on 29-07-2017.
 */

public class UserDetails {


    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("verified")
    @Expose
    public Boolean verified;
    @SerializedName("provider")
    @Expose
    public String provider;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("shopName")
    @Expose
    public String shopName;

    public class NewAddress {

        @SerializedName("location")
        @Expose
        public String location;

    }

    public class NewCardNumber {

        @SerializedName("ifscCode")
        @Expose
        public String ifscCode;
        @SerializedName("accountNum")
        @Expose
        public Long accountNum;
        @SerializedName("name")
        @Expose
        public String name;

    }










    @SerializedName("__v")
    @Expose
    public Integer v;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("contactNumber")
    @Expose
    public String contactNumber;
    @SerializedName("panCard")
    @Expose
    public String panCard;
    @SerializedName("birthDate")
    @Expose
    public String birthDate;
    @SerializedName("shopImgUrl")
    @Expose
    public String shopImgUrl;
    @SerializedName("shopPublicId")
    @Expose
    public String shopPublicId;
    @SerializedName("registrationNumber")
    @Expose
    public Integer registrationNumber;
    @SerializedName("storeType")
    @Expose
    public String storeType;
    @SerializedName("secondLicense")
    @Expose
    public Integer secondLicense;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("delivery")
    @Expose
    public Integer delivery;
    @SerializedName("deliveryCharge")
    @Expose
    public Integer deliveryCharge;
    @SerializedName("shipping")
    @Expose
    public String shipping;
    @SerializedName("shippingCharge")
    @Expose
    public String shippingCharge;
    @SerializedName("minOrderAmount")
    @Expose
    public Integer minOrderAmount;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("aadharCard")
    @Expose
    public String aadharCard;
    @SerializedName("storeOpenDate")
    @Expose
    public String storeOpenDate;
    @SerializedName("vendorType")
    @Expose
    public String vendorType;
    @SerializedName("additionalDetails")
    @Expose
    public String additionalDetails;
    @SerializedName("createdAt")
    @Expose
    public String createdAt;
    @SerializedName("role")
    @Expose
    public String role;
    @SerializedName("employeeEnable")
    @Expose
    public Boolean employeeEnable;
    @SerializedName("enable")
    @Expose
    public Boolean enable;
    @SerializedName("tracking")
    @Expose
    public Boolean tracking;
    @SerializedName("countryCode")
    @Expose
    public Integer countryCode;
    @SerializedName("businessHour")
    @Expose
    public List<BusinessHour> businessHour = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getPanCard() {
        return panCard;
    }

    public void setPanCard(String panCard) {
        this.panCard = panCard;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getShopImgUrl() {
        return shopImgUrl;
    }

    public void setShopImgUrl(String shopImgUrl) {
        this.shopImgUrl = shopImgUrl;
    }

    public String getShopPublicId() {
        return shopPublicId;
    }

    public void setShopPublicId(String shopPublicId) {
        this.shopPublicId = shopPublicId;
    }

    public Integer getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Integer registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public Integer getSecondLicense() {
        return secondLicense;
    }

    public void setSecondLicense(Integer secondLicense) {
        this.secondLicense = secondLicense;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getDelivery() {
        return delivery;
    }

    public void setDelivery(Integer delivery) {
        this.delivery = delivery;
    }

    public Integer getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Integer deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public Integer getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(Integer minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAadharCard() {
        return aadharCard;
    }

    public void setAadharCard(String aadharCard) {
        this.aadharCard = aadharCard;
    }

    public String getStoreOpenDate() {
        return storeOpenDate;
    }

    public void setStoreOpenDate(String storeOpenDate) {
        this.storeOpenDate = storeOpenDate;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(String additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getEmployeeEnable() {
        return employeeEnable;
    }

    public void setEmployeeEnable(Boolean employeeEnable) {
        this.employeeEnable = employeeEnable;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Boolean getTracking() {
        return tracking;
    }

    public void setTracking(Boolean tracking) {
        this.tracking = tracking;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public List<BusinessHour> getBusinessHour() {
        return businessHour;
    }

    public void setBusinessHour(List<BusinessHour> businessHour) {
        this.businessHour = businessHour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name1) {
        this.name = name1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email1) {
        this.email = email1;
    }


    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber1) {
        this.contactNumber = contactNumber1;
    }

}


