package com.relinns.micra_vendor.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Relinns Technologies on 26-07-2017.
 */

public class Login {
/*
    @SerializedName("token")
    @Expose
    public String token;*/

   @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }




}
