package com.relinns.micra_vendor.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.relinns.micra_vendor.DataModel.Homeaddress;
import com.relinns.micra_vendor.DataModel.StoreAddr;

import java.util.ArrayList;

/**
 * Created by Relinns Technologies on 27-07-2017.
 */

public class GlobalData {

    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private static final String USER_PREFS = "PREFS1";
    public ArrayList<Homeaddress> homeaddr=new ArrayList<>();
    public ArrayList<StoreAddr> storeaddr=new ArrayList<>();

    public GlobalData()
    {

    }
    public GlobalData(Context context) {
        appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        prefsEditor = appSharedPrefs.edit();
    }

    String token="token";
    String A_username="username";
    String A_password="password";
    String uname="name";
    String uemail="email";
    String ucontact_number="contact_number";
    String id="user_id";

    public String gettoken(){
        return appSharedPrefs.getString(token,"");
    }
    public void setUser_id(String id){
        prefsEditor.putString(id,id).commit();
    }
    public String getUser_id(){
        return appSharedPrefs.getString(id,"");
    }
    public void settoken(String token1){
        prefsEditor.putString(token,token1).commit();
    }
    public String getA_username(){
        return appSharedPrefs.getString(A_username,"");
    }
    public void setA_username(String username1){
        prefsEditor.putString(A_username,username1).commit();
    }
    public String getA_password(){
        return appSharedPrefs.getString(A_password,"");
    }
    public void setA_password(String password1){prefsEditor.putString(A_password,password1).commit();
    }
    public String getUname(){
        return appSharedPrefs.getString(uname,"");
    }
    public void setUname(String uname1){
        prefsEditor.putString(uname,uname1).commit();
    }

    public String getUemail(){
        return appSharedPrefs.getString(uemail,"");
    }
    public void setUemail(String uemail1){
        prefsEditor.putString(uemail,uemail1).commit();
    }

    public String getUcontact_number(){
        return appSharedPrefs.getString(ucontact_number,"");
    }
    public void setUcontact_number(String ucontact1){
        prefsEditor.putString(ucontact_number,ucontact1).commit();
    }


}
