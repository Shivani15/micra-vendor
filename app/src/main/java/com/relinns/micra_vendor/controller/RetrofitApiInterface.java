package com.relinns.micra_vendor.controller;

import com.relinns.micra_vendor.DataModel.Login;
import com.relinns.micra_vendor.DataModel.UserDetails;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;


/**
 * Created by Relinns Technologies on 26-07-2017.
 */

public interface RetrofitApiInterface {



    @POST("auth/local?")
    Call<Login> temptest(@QueryMap Map<String, String> fdsgbdsh);


    @GET("api/users/me?")
   // Call<UserDetails> UserDetail(@Header("Accept") String first, @Header("Authorization") String second);
    Call<UserDetails> UserDetails(@HeaderMap Map<String, String> headers);


    @POST("api/users/")
        // Call<UserDetails> UserDetail(@Header("Accept") String first, @Header("Authorization") String second);
    Call<Login> Signup(@HeaderMap Map<String, String> headers,@Body Map<String, String> fdsgbdsh);

}
