package com.relinns.micra_vendor.controller;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Relinns Technologies on 26-07-2017.
 */

public class RetrofitAdapter {

    private static Retrofit retrofit;
    private static final String BASE_URL = "http://apitest.micraapi.com/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
