package com.relinns.micra_vendor.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Adapter.BlockUsers_Adapter;
import com.relinns.micra_vendor.R;


public class Blocked_user_Activity extends AppCompatActivity implements View.OnClickListener {
    ListView blockuserLV;
    BlockUsers_Adapter blockUsers_adapter;
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_user);
        blockuserLV=(ListView)findViewById(R.id.blockuserLV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        blockUsers_adapter=new BlockUsers_Adapter(Blocked_user_Activity.this);
        blockuserLV.setAdapter(blockUsers_adapter);

        backRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case  R.id.backRL:

                onBackPressed();


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Blocked_user_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
