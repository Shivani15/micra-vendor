package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class OrderDetailAccepted_Activity extends Activity implements View.OnClickListener {
    RelativeLayout backRL;
    Button assignemployBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detailaccepted);

        backRL=(RelativeLayout)findViewById(R.id.backRL);
        assignemployBT=(Button)findViewById(R.id.assignemployBT);

        backRL.setOnClickListener(this);
        assignemployBT.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.assignemployBT:
                Intent intent=new Intent(OrderDetailAccepted_Activity.this,AssignEmployList_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        OrderDetailAccepted_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
