package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.relinns.micra_vendor.Fragment.MyProfileFragment;
import com.relinns.micra_vendor.Fragment.Setting_Fragment;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class AddProfile_Address_Activity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout backRL;
    EditText homename,streetname,landmark,city,state,pin;
    TextView save;
    Spinner country_spin;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_addprofileaddress);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        homename=(EditText)findViewById(R.id.homename);
        streetname=(EditText)findViewById(R.id.streetname);
        landmark=(EditText)findViewById(R.id.landmark);
        city=(EditText)findViewById(R.id.city);
        state=(EditText)findViewById(R.id.state);
        pin=(EditText)findViewById(R.id.pin);
        country_spin=(Spinner)findViewById(R.id.country);
        save=(TextView)findViewById(R.id.save);
        backRL.setOnClickListener(this);
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for( Locale loc : locale ){
            country = loc.getDisplayCountry();
            if( country.length() > 0 && !countries.contains(country) ){
                countries.add( country );
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, countries);
        country_spin.setAdapter(adapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = ProgressDialog.show(AddProfile_Address_Activity.this, "", "Please wait");
                progressDialog.setCancelable(true);
                addAddressApi();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();


                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AddProfile_Address_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }


    public void addAddressApi() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        Map<String, String> jsonParams = new HashMap<String, String>();

        jsonParams.put("user", new GlobalData(getApplicationContext()).getUser_id());
        jsonParams.put("homeName", homename.getText().toString());
        jsonParams.put("streetName", streetname.getText().toString());
        jsonParams.put("landmark", landmark.getText().toString());
        jsonParams.put("city", city.getText().toString());
        jsonParams.put("state", state.getText().toString());
        jsonParams.put("country", country_spin.getSelectedItem().toString());
        jsonParams.put("pincode", pin.getText().toString());
        jsonParams.put("primaryAddress", "false");


        JsonObjectRequest postRequest = new JsonObjectRequest( Request.Method.POST, "http://apitest.micraapi.com/api/addresses",

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Alerter.create(AddProfile_Address_Activity.this)
                                .setTitle("Micra")
                                .setText("Address added successfully!")
                                .setBackgroundColor(R.color.colorAccent)
                                .show();
                            finish();





                        Log.i("response","response"+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //   Handle Error
                        progressDialog.dismiss();

                        Log.i("response","error"+volleyError);
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        }
                        Alerter.create(AddProfile_Address_Activity.this)
                                .setTitle("Micra")
                                .setText(message)
                                .setBackgroundColor(R.color.colorAccent)
                                .show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", "Bearer "+ new GlobalData(getApplicationContext()).gettoken());

                return headers;
            }
        };
        requestQueue.add(postRequest);
    }
}
