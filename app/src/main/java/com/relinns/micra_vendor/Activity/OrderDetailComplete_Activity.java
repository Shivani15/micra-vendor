package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class OrderDetailComplete_Activity extends Activity  implements View.OnClickListener{

    RatingBar ratingbarRB;
    RelativeLayout backRL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetailcompleted);

        ratingbarRB = (RatingBar) findViewById(R.id.ratingbarRB);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);
      /*  Drawable drawable = ratingbarRB.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#ff6e40"),PorterDuff.Mode.SRC_ATOP);
        ratingbarRB.setRating(3);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        OrderDetailComplete_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
