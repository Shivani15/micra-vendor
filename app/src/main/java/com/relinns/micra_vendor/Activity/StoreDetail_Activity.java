package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class StoreDetail_Activity extends Activity  implements View.OnClickListener{

    RelativeLayout backRL;
    TextView addnewTV,editTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storedetail);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        addnewTV=(TextView)findViewById(R.id.addnewTV);
        editTV=(TextView)findViewById(R.id.editTV);
        backRL.setOnClickListener(this);
        addnewTV.setOnClickListener(this);
        editTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;

            case R.id.addnewTV:
                Intent intent=new Intent(StoreDetail_Activity.this,Add_StoreAddress_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.editTV:
                Intent intent1=new Intent(StoreDetail_Activity.this,EditStore_Activity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        StoreDetail_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
