package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Adapter.ChatListAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 01-07-2017.
 */
public class ChatList_Activity extends Activity implements View.OnClickListener{
    ListView chatlistLV;
    ChatListAdapter chatListAdapter;
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatlist);
        chatlistLV=(ListView)findViewById(R.id.chatlistLV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);
        chatListAdapter=new ChatListAdapter(ChatList_Activity.this);
        chatlistLV.setAdapter(chatListAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ChatList_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
