package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 05-07-2017.
 */
public class AddPermissions_Activity extends Activity  implements View.OnClickListener {

    RelativeLayout backRL;
    TextView vewpermisionTV;
    Button checkdetail_BT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addpermissions);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        vewpermisionTV=(TextView)findViewById(R.id.vewpermisionTV);
        checkdetail_BT=(Button)findViewById(R.id.checkdetail_BT);
        backRL.setOnClickListener(this);
        checkdetail_BT.setOnClickListener(this);
        vewpermisionTV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.vewpermisionTV:
                onBackPressed();

                break;

            case R.id.checkdetail_BT:

                Intent intent=new Intent(AddPermissions_Activity.this,PermissionEmploy_DetailActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AddPermissions_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
