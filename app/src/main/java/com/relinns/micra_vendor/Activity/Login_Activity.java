package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.relinns.micra_vendor.DataModel.Login;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.relinns.micra_vendor.controller.RetrofitAdapter;
import com.relinns.micra_vendor.controller.RetrofitApiInterface;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;


public class Login_Activity extends Activity implements View.OnClickListener {

    LinearLayout loginBT;
    EditText emailET, passwordET;
    ProgressDialog progressDialog;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_);
        emailET = (EditText) findViewById(R.id.emailET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        loginBT = (LinearLayout) findViewById(R.id.loginBT);
        loginBT.setOnClickListener(this);

//        emailET.setText("shubh@gmail.com");
//        passwordET.setText("123456");

        // emailET.setText(new GlobalData(Login_Activity.this).getA_username());
       // passwordET.setText(new GlobalData(Login_Activity.this).getA_password());
    }

    public void open_reg(View view) {

        Intent intent = new Intent(Login_Activity.this, Signup_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public void open_forgot(View view) {

        Intent intent = new Intent(Login_Activity.this, Forgot_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public void backpress(View view) {
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Login_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBT:

                if (emailET.getText().toString().equals("")) {
                    Alerter.create(this)
                            .setTitle("Micra")
                            .setText("Please enter email")
                            .setBackgroundColor(R.color.colorAccent)
                            .show();
                }
               else if (!emailET.getText().toString().matches(emailPattern)) {
                    Alerter.create(this)
                            .setTitle("Micra")
                            .setText("Please enter valid email")
                            .setBackgroundColor(R.color.colorAccent)
                            .show();

                }
                else if (passwordET.getText().toString().equals("")) {
                    Alerter.create(this)
                            .setTitle("Micra")
                            .setText("Please enter password")
                            .setBackgroundColor(R.color.colorAccent)
                            .show();
                }

                else {
                   /* Intent intent = new Intent(Login_Activity.this, My_Products_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);*/
                    progressDialog = ProgressDialog.show(Login_Activity.this, "", "Please wait");
                    progressDialog.setCancelable(true);
                    loginApi();
                }


                break;
        }

    }

    private void loginApi() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", emailET.getText().toString());
        params.put("password", passwordET.getText().toString());

        RetrofitApiInterface service = RetrofitAdapter.getRetrofitInstance().create(RetrofitApiInterface.class);
        // Call<Example> call = service.temptest("shubh@gmail.com","123456");
        Call<Login> call = service.temptest(params);
        Log.wtf("request", call.request().url() + "");

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    Log.d("API_Response", "login Response : " + new Gson().toJson(response.body()));
                    String token = response.body().getToken();
                    new GlobalData(Login_Activity.this).settoken(token);
                    new GlobalData(Login_Activity.this).setA_username(emailET.getText().toString());
                    new GlobalData(Login_Activity.this).setA_password(passwordET.getText().toString());

                    Intent intent = new Intent(Login_Activity.this, My_Products_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else {
                    progressDialog.dismiss();
                    Log.e("API_Response", "login Response : " + new Gson().toJson(response.errorBody().toString()));

                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());

                        Alerter.create(Login_Activity.this)
                                .setTitle("Micra")
                                .setText(jObjError.getString("message"))
                                .setBackgroundColor(R.color.colorAccent)
                                .show();
                        // Toast.makeText(Login_Activity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {


            }

        });


    }
}
