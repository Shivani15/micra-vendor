package com.relinns.micra_vendor.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.relinns.micra_vendor.R;


public class Forgot_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_);
    }

    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Forgot_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

}
