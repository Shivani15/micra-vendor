package com.relinns.micra_vendor.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.relinns.micra_vendor.R;
import com.tapadoo.alerter.Alerter;

public class verification_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_);
    }

    public void open_verification(View v){
        Alerter.create(this)
                .setTitle("Alert Title")
                .setText("Alert text...")
                .setBackgroundColor(R.color.colorAccent)
                .show();
    }

    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        verification_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

}
