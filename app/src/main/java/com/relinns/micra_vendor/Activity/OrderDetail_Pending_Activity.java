package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class OrderDetail_Pending_Activity extends Activity implements View.OnClickListener{
    RelativeLayout backRL;
    Button gmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderdetail_pending);
        backRL=(RelativeLayout)findViewById(R.id.backRL);


        backRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        OrderDetail_Pending_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
