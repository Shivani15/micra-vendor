package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 03-07-2017.
 */
public class AddEmployee_Activity extends Activity implements View.OnClickListener {
RelativeLayout backRL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addemployee);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AddEmployee_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
