package com.relinns.micra_vendor.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.relinns.micra_vendor.R;


public class Splashscreen extends AppCompatActivity {

    static int progress;
    int progressStatus = 0;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        progress = 0;

        //---do some work in background thread--
        new Thread(new Runnable() {
            public void run() {
                //---do some work here--
                while (progressStatus < 5) {
                    progressStatus = doSomeWork();
                }


                //---hides the progress bar--
                handler.post(new Runnable() {
                    public void run() {                        //---0 - VISIBLE; 4 - INVISIBLE; 8 - GONE--
                        // progressBar.setVisibility(View.GONE);

                            Intent intent = new Intent(Splashscreen.this, getStart_Activity.class);
                            startActivity(intent);
                        overridePendingTransition(R.anim.enter,R.anim.exit);
                            finish();
                    }
                });
            }


            //--do some long running work here--
            private int doSomeWork() {

                try {
                    //---simulate doing some work--
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                return ++progress;
            }
        }).start();


    }
}
