package com.relinns.micra_vendor.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.relinns.micra_vendor.Adapter.AddFieldAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 23-06-2017.
 */
public class AddNewProduct_Activity extends Activity implements View.OnClickListener {
    RelativeLayout backRL,plusRL,deleteRL,taxRL;
    ImageView cameraIV, galleryIV, productIV;
    private static final int CAMERA_REQUEST = 1888;
    private static final int GALLERY_REQUEST = 1999;
    Uri selectedImageUri;
    ListView addfieldLV;
    AddFieldAdapter addFieldAdapter;
    static int count=1;
    static int checkcount=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewproduct);
        backRL = (RelativeLayout) findViewById(R.id.backRL);
        plusRL = (RelativeLayout) findViewById(R.id.plusRL);
        deleteRL = (RelativeLayout) findViewById(R.id.deleteRL);
        taxRL = (RelativeLayout) findViewById(R.id.taxRL);
        cameraIV = (ImageView) findViewById(R.id.cameraIV);
        productIV = (ImageView) findViewById(R.id.productIV);
        galleryIV = (ImageView) findViewById(R.id.galleryIV);
        addfieldLV=(ListView)findViewById(R.id.addfeildLV);
        backRL.setOnClickListener(this);
        cameraIV.setOnClickListener(this);
        galleryIV.setOnClickListener(this);
        plusRL.setOnClickListener(this);
        deleteRL.setOnClickListener(this);
        taxRL.setOnClickListener(this);

        addFieldAdapter=new AddFieldAdapter(AddNewProduct_Activity.this, count);
        addfieldLV.setAdapter(addFieldAdapter);
        setListViewHeight(addfieldLV);

    }

    public void getdata(){
        checkcount=  count++;
        addFieldAdapter=new AddFieldAdapter(AddNewProduct_Activity.this, checkcount);
        addfieldLV.setAdapter(addFieldAdapter);
        setListViewHeight(addfieldLV);

    }
    public void minus_data(){

        checkcount=  count--;
        addFieldAdapter=new AddFieldAdapter(AddNewProduct_Activity.this, checkcount);
        addfieldLV.setAdapter(addFieldAdapter);
        setListViewHeight(addfieldLV);

    }

    public static void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;

            case R.id.cameraIV:

                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }

                break;


            case R.id.galleryIV:
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_REQUEST);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, GALLERY_REQUEST);
                }

            case R.id.plusRL:
                if (checkcount<4){
                checkcount =  ++count;
                addFieldAdapter=new AddFieldAdapter(AddNewProduct_Activity.this, checkcount);
                addfieldLV.setAdapter(addFieldAdapter);
                setListViewHeight(addfieldLV);}
                else{

                }
                break;

            case R.id.deleteRL:
                if (checkcount==1){

                }
                else{
                checkcount =  --count;
                addFieldAdapter=new AddFieldAdapter(AddNewProduct_Activity.this, checkcount);
                addfieldLV.setAdapter(addFieldAdapter);
                setListViewHeight(addfieldLV);}

                break;


            case R.id.taxRL:

                Intent intent=new Intent(AddNewProduct_Activity.this,AddTax_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AddNewProduct_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {

            Bitmap photo = (Bitmap) data.getExtras().get("data");
            productIV.setImageBitmap(photo);
        }






        else if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            if (resultCode == RESULT_OK) {
                if (requestCode == GALLERY_REQUEST) {
                    // Get the url from data
                    selectedImageUri = data.getData();


                    if (!selectedImageUri.equals("")) {

                        Glide
                                .with(AddNewProduct_Activity.this)
                                .load(selectedImageUri)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .centerCrop()
                                .crossFade()
                                .into(productIV);

                    }

                }
            }

        } else {
            Toast.makeText(this, "no response", Toast.LENGTH_SHORT).show();
        }
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }


        } else if (requestCode == GALLERY_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST);
            }
        } else {

            Toast.makeText(this, "Gallery permission denied", Toast.LENGTH_LONG).show();

        }

    }

}



