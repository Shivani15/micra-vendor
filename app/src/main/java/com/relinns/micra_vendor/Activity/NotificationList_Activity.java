package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Adapter.NotificationList_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class NotificationList_Activity extends Activity implements View.OnClickListener{

    ListView notificationLV;
    NotificationList_Adapter notificationList_adapter;
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificationlist);
        notificationLV=(ListView)findViewById(R.id.notificationLV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);

        notificationList_adapter=new NotificationList_Adapter(NotificationList_Activity.this);
        notificationLV.setAdapter(notificationList_adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        NotificationList_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
