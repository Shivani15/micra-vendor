package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Adapter.PermissionList_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 04-07-2017.
 */
public class PermissionList_Activity extends Activity implements View.OnClickListener {

    ListView permissionLV;
    PermissionList_Adapter permissionList_adapter;
    RelativeLayout backRL;
    TextView addnewTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissionlist);
        permissionLV=(ListView)findViewById(R.id.permissionLV);
        addnewTV=(TextView)findViewById(R.id.addnewTV);
        backRL=(RelativeLayout)findViewById(R.id.backRL);

        backRL.setOnClickListener(this);
        addnewTV.setOnClickListener(this);

        permissionList_adapter=new PermissionList_Adapter(PermissionList_Activity.this);
        permissionLV.setAdapter(permissionList_adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.addnewTV:
                Intent intent= new Intent(PermissionList_Activity.this,AddPermissions_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        PermissionList_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
