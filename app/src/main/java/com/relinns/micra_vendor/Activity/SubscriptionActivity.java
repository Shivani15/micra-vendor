package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Adapter.InvoiceListAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 05-07-2017.
 */
public class SubscriptionActivity extends Activity implements View.OnClickListener{

    RelativeLayout backRL;
    LinearLayout planLL,invoiceLL,paymentLL,planlayout,paymentlayout;
    TextView planTV,invoiceTV,paymentTV;
    ListView invoiceLV;
    InvoiceListAdapter invoiceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        planLL=(LinearLayout)findViewById(R.id.planLL);
        paymentLL=(LinearLayout)findViewById(R.id.paymentLL);
        invoiceLL=(LinearLayout)findViewById(R.id.invoiceLL);

        planlayout=(LinearLayout)findViewById(R.id.planlayout);
        paymentlayout=(LinearLayout)findViewById(R.id.paymentlayout);
        invoiceLV=(ListView)findViewById(R.id.invoiceLV);

        planTV=(TextView)findViewById(R.id.planTV);
        invoiceTV=(TextView)findViewById(R.id.invoiceTV);
        paymentTV=(TextView)findViewById(R.id.paymentTV);

        backRL.setOnClickListener(this);
        planLL.setOnClickListener(this);
        paymentLL.setOnClickListener(this);
        invoiceLL.setOnClickListener(this);

        invoiceListAdapter=new InvoiceListAdapter(SubscriptionActivity.this);
        invoiceLV.setAdapter(invoiceListAdapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;

            case R.id.planLL:
                planlayout.setVisibility(View.VISIBLE);
                invoiceLV.setVisibility(View.GONE);
                paymentlayout.setVisibility(View.GONE);



                planLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                paymentLL.setBackgroundColor(getResources().getColor(R.color.white));
                invoiceLL.setBackgroundColor(getResources().getColor(R.color.white));

                planTV.setTextColor(getResources().getColor(R.color.white));
                invoiceTV.setTextColor(getResources().getColor(R.color.black));
                paymentTV.setTextColor(getResources().getColor(R.color.black));


                break;

            case R.id.invoiceLL:

                planlayout.setVisibility(View.GONE);
                invoiceLV.setVisibility(View.VISIBLE);
                paymentlayout.setVisibility(View.GONE);

                invoiceLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                planLL.setBackgroundColor(getResources().getColor(R.color.white));
                paymentLL.setBackgroundColor(getResources().getColor(R.color.white));

                invoiceTV.setTextColor(getResources().getColor(R.color.white));
                planTV.setTextColor(getResources().getColor(R.color.black));
                paymentTV.setTextColor(getResources().getColor(R.color.black));

                break;
            case R.id.paymentLL:

                planlayout.setVisibility(View.GONE);
                invoiceLV.setVisibility(View.GONE);
                paymentlayout.setVisibility(View.VISIBLE);


                paymentLL.setBackground(getResources().getDrawable(R.drawable.corner_not_round_button));
                planLL.setBackgroundColor(getResources().getColor(R.color.white));
                invoiceLL.setBackgroundColor(getResources().getColor(R.color.white));

                paymentTV.setTextColor(getResources().getColor(R.color.white));
                planTV.setTextColor(getResources().getColor(R.color.black));
                invoiceTV.setTextColor(getResources().getColor(R.color.black));

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        SubscriptionActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
