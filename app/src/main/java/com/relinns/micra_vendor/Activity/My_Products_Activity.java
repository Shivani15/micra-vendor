package com.relinns.micra_vendor.Activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.relinns.micra_vendor.Adapter.NavigationHome_Adapter;
import com.relinns.micra_vendor.Fragment.Allowance_Fragment;
import com.relinns.micra_vendor.Fragment.LeaveApplication_Fragment;
import com.relinns.micra_vendor.Fragment.MessageListFragment;
import com.relinns.micra_vendor.Fragment.MyEmployeeList_Fragment;
import com.relinns.micra_vendor.Fragment.MyFavourite_Fragment;
import com.relinns.micra_vendor.Fragment.MyOrderList_Fragment;
import com.relinns.micra_vendor.Fragment.MyProfileFragment;
import com.relinns.micra_vendor.Fragment.MyWallet_Fragment;
import com.relinns.micra_vendor.Fragment.OverViewFragment;
import com.relinns.micra_vendor.Fragment.ProductList_Fragment;

import com.relinns.micra_vendor.Fragment.Setting_Fragment;
import com.relinns.micra_vendor.Fragment.WebMicra_Fragment;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.model.NavigationHome_Model;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Relinns Technologies on 19-06-2017.
 */
public class My_Products_Activity extends AppCompatActivity implements View.OnClickListener {
    ListView mDrawerList;
    RelativeLayout mainLL;
    Toolbar mToolbar;
    ActionBar actionBar;
    DrawerLayout mDrawerLayout;
    TextView titletext,editTV,adnew_emplyTV;
    ArrayList<NavigationHome_Model> navigationHome_models;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private float lastTranslate = 0.0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myproduct_activity);

        titletext = (TextView) findViewById(R.id.titleTV1);
        editTV = (TextView) findViewById(R.id.editTV);
        adnew_emplyTV = (TextView) findViewById(R.id.adnew_emplyTV);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mainLL = (RelativeLayout) findViewById(R.id.mainLL);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        editTV.setOnClickListener(this);
        adnew_emplyTV.setOnClickListener(this);

        actionBar = getSupportActionBar();
        setSupportActionBar(mToolbar);
        navigationHome_models = new ArrayList<NavigationHome_Model>();

        navigationHome_models.add(new NavigationHome_Model(R.drawable.overview, "Overview"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_product, "My Products"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_order, "My Orders"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.my_profile, "My Profile"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.favourites, "Favourites"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.wallet, "My Wallet"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.message, "Message"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.leaveapp, "Leave Applications"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.allowance, "Allowance"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.webmicra, "Login Web Micra"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.store_employee, "Store Employee"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.settings, "Settings"));
        navigationHome_models.add(new NavigationHome_Model(R.drawable.logout, "Logout"));


        LayoutInflater inflater = getLayoutInflater();

        View listHeaderView = inflater.inflate(R.layout.header_list, null, false);

        ImageView drawer_profileIV = (CircleImageView) listHeaderView.findViewById(R.id.drawer_profileIV);
        TextView drawer_profilenameTV = (TextView) listHeaderView.findViewById(R.id.drawer_profilenameTV);

        // Glide.with(My_Products_Activity.this).load(persnl_image).centerCrop().crossFade().into(drawer_profileIV);
        //  drawer_profilenameTV.setText(persnl_name);

        mDrawerList.addHeaderView(listHeaderView);


        mDrawerList.setAdapter(new NavigationHome_Adapter(navigationHome_models, My_Products_Activity.this));


       // productlist_Fragment();
        overview_fragment();


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                null,  /* nav drawer icon to replace 'Up' caret */

                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                InputMethodManager inputMethodManager = (InputMethodManager) My_Products_Activity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(My_Products_Activity.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);

                InputMethodManager inputMethodManager = (InputMethodManager) My_Products_Activity.this
                        .getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(My_Products_Activity.this.getCurrentFocus().getWindowToken(), 0);

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                float moveFactor = (mDrawerList.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mainLL.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    mainLL.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }

          /*  super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();*/
        };


        getSupportActionBar().setTitle("");


        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.navigation);
        mToolbar.setTitle("");
        mToolbar.setSubtitle("");


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(Navigation_Home_First.this, "=" + position, Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:

                        break;

                    case 1:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);

                        overview_fragment();

                        break;

                    case 2:


                        productlist_Fragment();



                        break;
                    case 3:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);

                        my_order_Freagment();

                        break;

                    case 4:

                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.VISIBLE);
                        adnew_emplyTV.setVisibility(View.GONE);

                        my_profile_fragment();

                       /* Intent intent = new Intent(My_Products_Activity.this, Add_StoreAddress_Activity.class);
                        startActivity(intent);*/

                        break;

                    case 5:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);

                        myfavourite_Fragment();
                        break;

                    case 6:
                        mDrawerLayout.closeDrawers();
                        editTV.setVisibility(View.GONE);
                        adnew_emplyTV.setVisibility(View.GONE);
                        My_wallet_fragment();


                        break;
                    case 7:

                      messagelist_Fragment();

                        break;

                    case 8:

                        Leaveapplication_Fragment();



                        break;
                    case 9:
                       Allowance_Adapter();

                        break;
                    case 10:
                      webmicra_Fragment();

                        break;
                    case 11:

                        employeelist_Fragment();


                        break;
                    case 12:
                        Settingfragment();
                        break;

                    case 13:
                        Intent intent=new Intent(My_Products_Activity.this,Login_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter,R.anim.exit);
                        break;

                }
            }

            private void My_wallet_fragment() {
                Fragment favouriteFragment = new MyWallet_Fragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
                fragmentTransaction.commit();
            }


        });


    }

    private void overview_fragment() {
        Fragment favouriteFragment = new OverViewFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();

    }

    private void Settingfragment() {
        Fragment favouriteFragment = new Setting_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();


        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);

    }

    private void webmicra_Fragment() {

        Fragment favouriteFragment = new WebMicra_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();


        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);
    }

    private void employeelist_Fragment() {

        Fragment favouriteFragment = new MyEmployeeList_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();


        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.VISIBLE);


    }

    private void Allowance_Adapter() {

        Fragment favouriteFragment = new Allowance_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();

        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);
    }

    private void Leaveapplication_Fragment() {

        Fragment favouriteFragment = new LeaveApplication_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();


        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);
    }

    private void messagelist_Fragment() {
        Fragment favouriteFragment = new MessageListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();

        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);
    }

    private void myfavourite_Fragment() {
        Fragment favouriteFragment = new MyFavourite_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();

    }

    private void my_profile_fragment() {

        Fragment favouriteFragment = new MyProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();


    }

    private void my_order_Freagment() {



        Fragment favouriteFragment = new MyOrderList_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();



    }

    private void productlist_Fragment() {

        Fragment favouriteFragment = new ProductList_Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, favouriteFragment, "home");
        fragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
        editTV.setVisibility(View.GONE);
        adnew_emplyTV.setVisibility(View.GONE);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editTV:
                Intent intent=new Intent(My_Products_Activity.this,EditProfile_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
            case R.id.adnew_emplyTV:
                Intent intent1=new Intent( My_Products_Activity.this,AddEmployee_Activity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }

        }
        return super.onOptionsItemSelected(item);
    }


    public void settitleactivity(String title){
        //    getSupportActionBar().setTitle(title);
        titletext.setText(title);
    }

    @Override
    public void onBackPressed() {
        exit();
    }

    public void exit() {
        AlertDialog.Builder a = new AlertDialog.Builder(My_Products_Activity.this);
        a.setMessage("Do You Want To Exit ");
        a.setCancelable(true);
        a.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent setIntent = new Intent(Intent.ACTION_MAIN);
                setIntent.addCategory(Intent.CATEGORY_HOME);
                setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(setIntent);
                My_Products_Activity.this.finish();
            }
        });
        a.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alertDialog = a.create();
        alertDialog.show();
    }
}
