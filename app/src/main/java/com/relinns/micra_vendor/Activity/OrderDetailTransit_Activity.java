package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class OrderDetailTransit_Activity extends Activity implements View.OnClickListener {
    RelativeLayout backRL;
    Button trackBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetailtransit);
        backRL = (RelativeLayout) findViewById(R.id.backRL);
        trackBT = (Button) findViewById(R.id.trackBT);
        trackBT.setOnClickListener(this);
        backRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;

            case R.id.trackBT:
                Intent intent = new Intent(OrderDetailTransit_Activity.this, MapsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        OrderDetailTransit_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
