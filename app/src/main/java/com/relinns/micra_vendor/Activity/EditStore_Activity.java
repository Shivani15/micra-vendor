package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.relinns.micra_vendor.Adapter.AddFieldAdapter;
import com.relinns.micra_vendor.Adapter.EditStoreBusinnessListAdapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 30-06-2017.
 */
public class EditStore_Activity extends Activity implements View.OnClickListener{

    RelativeLayout backRL;
    ListView bussinessLV;
    EditStoreBusinnessListAdapter businnessListAdapter;
    static int count=1;
    ImageView addicon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editstoredetail);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        bussinessLV=(ListView) findViewById(R.id.bussinessLV);
        addicon=(ImageView)findViewById(R.id.addicon);
        backRL.setOnClickListener(this);
        addicon.setOnClickListener(this);

        businnessListAdapter=new EditStoreBusinnessListAdapter(EditStore_Activity.this,count);
        bussinessLV.setAdapter(businnessListAdapter);
        setListViewHeight(bussinessLV);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;

            case R.id.addicon:


                if (count<7){
                count=++count;

                businnessListAdapter=new EditStoreBusinnessListAdapter(EditStore_Activity.this,count);
                bussinessLV.setAdapter(businnessListAdapter);
                setListViewHeight(bussinessLV);
                }
                else{}

                break;
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        EditStore_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    public void getdeletemethod(){

        if (count==1){}
        else{
        count=--count;
        businnessListAdapter=new EditStoreBusinnessListAdapter(EditStore_Activity.this,count);
        bussinessLV.setAdapter(businnessListAdapter);
        setListViewHeight(bussinessLV);}
    }


    public static void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
