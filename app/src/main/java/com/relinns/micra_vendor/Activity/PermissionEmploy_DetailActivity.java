package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 05-07-2017.
 */
public class PermissionEmploy_DetailActivity extends Activity implements View.OnClickListener {
    RelativeLayout backRL,trakRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissionemploy_detail);

        backRL=(RelativeLayout)findViewById(R.id.backRL);
        trakRL=(RelativeLayout)findViewById(R.id.trakRL);
        backRL.setOnClickListener(this);
        trakRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.backRL:

                onBackPressed();

                break;

            case R.id.trakRL:

                Intent intent=new Intent(PermissionEmploy_DetailActivity.this,TrackOrder_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        PermissionEmploy_DetailActivity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
