package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Adapter.RequestPaymentListAdapter;
import com.relinns.micra_vendor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Relinns Technologies on 07-07-2017.
 */

public class RequestMoney_Activity extends Activity  implements View.OnClickListener{
    ListView requestLV;
    RequestPaymentListAdapter requestPaymentListAdapter;
    RelativeLayout backRL;
    LinearLayout topLL;

    private View stickyViewSpacer;
    private int MAX_ROWS = 20;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestmoney);
        requestLV=(ListView) findViewById(R.id.requestLV);
        backRL=(RelativeLayout) findViewById(R.id.backRL);
        topLL=(LinearLayout) findViewById(R.id.topLL);

        backRL.setOnClickListener(this);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = inflater.inflate(R.layout.list_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);

        requestLV.addHeaderView(listHeader);


        requestLV.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                /* Check if the first item is already reached to top.*/
                if (requestLV.getFirstVisiblePosition() == 0) {
                    View firstChild = requestLV.getChildAt(0);
                    int topY = 0;
                    if (firstChild != null) {
                        topY = firstChild.getTop();
                    }

                    // int heroTopY = stickyViewSpacer.getTop();
                    // stickyView.setY(Math.max(0, heroTopY + topY));

                    /* Set the image to scroll half of the amount that of ListView */
                    topLL.setY(topY * 0.5f);
                }
            }
        });


        /* Populate the ListView with sample data */
        /*List<String> modelList = new ArrayList<>();
        for (int i = 0; i < MAX_ROWS; i++) {
            modelList.add("List item " + i);
        }*/

/*        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.list_row, modelList);
        listView.setAdapter(adapter);*/

        requestPaymentListAdapter=new RequestPaymentListAdapter(RequestMoney_Activity.this);
        requestLV.setAdapter(requestPaymentListAdapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        RequestMoney_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
