package com.relinns.micra_vendor.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.R;


public class Add_StoreAddress_Activity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout backRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__address);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.backRL:
              onBackPressed();
              break;
      }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Add_StoreAddress_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }


}
