package com.relinns.micra_vendor.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.relinns.micra_vendor.Adapter.custom_swip_Adapter;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.model.image_model;

import java.util.ArrayList;

public class getStart_Activity extends AppCompatActivity implements View.OnClickListener{

    ViewPager viewPager;
    custom_swip_Adapter adapter;
    ArrayList<image_model> imageurl;
    ImageView image1,image2,image3;
    LinearLayout loginLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_start_);
        viewPager = (ViewPager)findViewById(R.id.viewpager1);
        loginLL=(LinearLayout)findViewById(R.id.loginLL);
        loginLL.setOnClickListener(this);

        image1 = (ImageView)findViewById(R.id.image1);
        image2 = (ImageView)findViewById(R.id.image2);
        image3 = (ImageView)findViewById(R.id.image3);

        imageurl = new ArrayList<>();
        imageurl.add(new image_model(R.drawable.get_started));
        imageurl.add(new image_model(R.drawable.splash_logo));
        imageurl.add(new image_model(R.drawable.get_started));

        image1.setImageResource(R.drawable.circle_large);
        image2.setImageResource(R.drawable.circle_small);
        image3.setImageResource(R.drawable.circle_small);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position){

                    case 0:
                        image1.setImageResource(R.drawable.circle_large);
                        image2.setImageResource(R.drawable.circle_small);
                        image3.setImageResource(R.drawable.circle_small);
                        break;

                    case 1:
                        image1.setImageResource(R.drawable.circle_small);
                        image2.setImageResource(R.drawable.circle_large);
                        image3.setImageResource(R.drawable.circle_small);
                        break;

                    case 2:
                        image1.setImageResource(R.drawable.circle_small);
                        image2.setImageResource(R.drawable.circle_small);
                        image3.setImageResource(R.drawable.circle_large);
                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        adapter = new custom_swip_Adapter(getStart_Activity.this,imageurl);
        viewPager.setAdapter(adapter);
    }
    public void open_signup(View view){

        Intent intent = new Intent(getStart_Activity.this,Signup_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter,R.anim.exit);

    }

    @Override
    public void onPause() {

        if(adapter != null)
        {
            adapter.notifyDataSetChanged();
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginLL:
                Intent intent = new Intent(getStart_Activity.this,Login_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exit);
                break;
        }
    }
}
