package com.relinns.micra_vendor.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.relinns.micra_vendor.Adapter.CountryCodeAdapter;
import com.relinns.micra_vendor.DataModel.Login;
import com.relinns.micra_vendor.DataModel.UserDetails;
import com.relinns.micra_vendor.R;
import com.relinns.micra_vendor.controller.GlobalData;
import com.relinns.micra_vendor.controller.RetrofitAdapter;
import com.relinns.micra_vendor.controller.RetrofitApiInterface;
import com.relinns.micra_vendor.model.CountryCode_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup_Activity extends AppCompatActivity {

    Dialog dialog;
    ListView listView;
    String items[],items1[];
    TextView c_code,line;
    EditText searchET;
    ArrayAdapter<String> itemsAdapter;
    CountryCodeAdapter countryCodeAdapter;
    ArrayList<CountryCode_Model>countryCode_models;
    EditText nameET,numberET,emailET,passwordET,confrmpassET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_);
        countryCode_models=new ArrayList<>();
        code();
        c_code = (TextView)findViewById(R.id.c_code);
        line = (TextView)findViewById(R.id.line);
        nameET=(EditText)findViewById(R.id.nameET);
        numberET=(EditText)findViewById(R.id.numberET);
        emailET=(EditText)findViewById(R.id.emailET);
        passwordET=(EditText)findViewById(R.id.passwordET);
        confrmpassET=(EditText)findViewById(R.id.confrmpassET);
        line_color();
         String android_id = Settings.Secure.getString(Signup_Activity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("deviceid",android_id);

    }


    public void open_verification(View view){

        signup_Api();



    }

    private void signup_Api() {


        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");
        params.put("Accept", "application/json");



        Map<String, String> params1 = new HashMap<String, String>();
        params1.put("name", nameET.getText().toString());
        params1.put("deviceId", Settings.Secure.getString(Signup_Activity.this.getContentResolver(),Settings.Secure.ANDROID_ID));
        params1.put("platform", "0");
        params1.put("latitude", "sa");
        params1.put("longitude", "fs");
        params1.put("countryCode", "91");
        params1.put("contactNumber", numberET.getText().toString());
        params1.put("email", emailET.getText().toString());
        params1.put("role", "seller");
        params1.put("vendorType", "Retailer");
        params1.put("password", passwordET.getText().toString());

        RetrofitApiInterface service = RetrofitAdapter.getRetrofitInstance().create(RetrofitApiInterface.class);

        Call<Login> call = service.Signup(params,params1);

        Log.wtf("request", call.request().url() + "");

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                // Log.d("API_Response", "UserDetail Response : " + new Gson().toJson(response.body()));
                //  Log.i("log", response.body());

                Log.d("Call request", call.request().toString());
                Log.d("Call request header", call.request().headers().toString());

                Log.d("Response raw header", response.headers().toString());
                Log.d("Response raw", String.valueOf(response.raw().body()));
                Log.d("Response code", String.valueOf(response.code()));

                if (response.isSuccessful()) {
                    Log.d("Response body", new Gson().toJson(response.body().toString()));

                    String token=    response.body().getToken();
                    new GlobalData(Signup_Activity.this).settoken(token);

                    Intent intent = new Intent(Signup_Activity.this,My_Products_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter,R.anim.exit);



                } else {
                    Log.d("Response errorBody", String.valueOf(response.errorBody()));
                    // Log.d("API_Response", "UserDetail Response  : " + new Gson().toJson(response.errorBody()));
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d("onFailure", t.toString());
                //t.getCause();

            }

        });


    }


    public void backpress(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Signup_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }

    void line_color(){
        SpannableString SpanString = new SpannableString("By clicking 'Register' you agree to Terms & Conditions and Privacy Policy");

        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 12, 22, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 36, 54, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.micra)), 59, 73, 0);

        line.setMovementMethod(LinkMovementMethod.getInstance());
        line.setText(SpanString, TextView.BufferType.SPANNABLE);
        line.setSelected(true);


    }

    void code(){

        try {
            JSONArray jsonArray=new JSONArray(loadJSONFromAsset());
            items=new String[jsonArray.length()];
            items1=new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject object=jsonArray.getJSONObject(i);
                Log.d("response",object+"");
                items[i]="+"+object.getString("code")+" "+object.getString("name");
                items1[i] = "+"+object.getString("code");
                String name=object.getString("name");
                String code=object.getString("code");
                Log.d("check",name  +code);

                countryCode_models.add(new CountryCode_Model(name,code));
            }





        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("isd.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public void open_code(View view) {
        // DialogWithBlurredBackgroundLauncher dialogWithBlurredBackgroundLauncher = new DialogWithBlurredBackgroundLauncher(MainActivity.this);
        dialog = new Dialog(Signup_Activity.this);
        // Include dialog.xml file

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes(); // retrieves the windows attributes

        lp.dimAmount=0.0f; // sets the dimming amount to zero

        dialog.getWindow().setAttributes(lp); // sets the updated windows attributes

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.countrydesign);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);



        listView=(ListView) dialog.findViewById(R.id.list);
        searchET=(EditText) dialog.findViewById(R.id.searchET);
        //listView.setFastScrollEnabled(true);

       // Log.d("modal",countryCode_models.toString());

        code();
        countryCodeAdapter =new CountryCodeAdapter(Signup_Activity.this,countryCode_models);
        listView.setAdapter(countryCodeAdapter);

        /* itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(itemsAdapter);*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

              c_code.setText(countryCode_models.get(position).getCode());
                dialog.dismiss();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                countryCodeAdapter =new CountryCodeAdapter(Signup_Activity.this,countryCode_models);
                listView.setAdapter(countryCodeAdapter);
            }
        });



        searchET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                //  Toast.makeText(getActivity(), cs.toString(), Toast.LENGTH_LONG).show();
                String text = searchET.getText().toString().toLowerCase(Locale.getDefault());
                countryCodeAdapter.filter(text);


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });



       /* cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });*/
        dialog.show();
        // dialogWithBlurredBackgroundLauncher.showDialog(dialog);
    }






}
