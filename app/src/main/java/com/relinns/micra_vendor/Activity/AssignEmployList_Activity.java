package com.relinns.micra_vendor.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.relinns.micra_vendor.Adapter.AssignEmployee_Adapter;
import com.relinns.micra_vendor.R;

/**
 * Created by Relinns Technologies on 29-06-2017.
 */
public class AssignEmployList_Activity extends Activity implements View.OnClickListener {
    ListView employlistLV;
    RelativeLayout backRL;
    AssignEmployee_Adapter employee_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignemploylist);
        backRL=(RelativeLayout)findViewById(R.id.backRL);
        backRL.setOnClickListener(this);
        employlistLV=(ListView)findViewById(R.id.employlistLV);
        employee_adapter=new AssignEmployee_Adapter(AssignEmployList_Activity.this);
        employlistLV.setAdapter(employee_adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AssignEmployList_Activity.this.overridePendingTransition(R.anim.exit2, R.anim.enter2);
    }
}
